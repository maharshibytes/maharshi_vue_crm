import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import router from './routes/route';
import axios from 'axios';
import Vuelidate from 'vuelidate'
import Toast from "vue-toastification";
import "vue-toastification/dist/index.css";
import HeaderSectionLayout from './components/comman/header-section'
import FooterSectionLayout from './components/comman/footer-section'

const Toastoptions = {
  // You can set your default options here
};

Vue.use(Toast,Toastoptions)
Vue.use(Vuelidate)
Vue.use(VueRouter)
Vue.component('myHeader', HeaderSectionLayout)
Vue.component('myFooter', FooterSectionLayout)

Vue.config.productionTip = false

axios.defaults.baseURL = 'http://local.crm-backend.in/api/';

new Vue({router,
  render: h => h(App),
}).$mount('#app')
