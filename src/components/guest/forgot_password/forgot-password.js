import GuestLeftSectionImage from '../../comman/guest_user/left-section.vue'
import $ from "jquery";

export default {
  name: 'Login',
  mounted: function() {

    $('#togglePassword').on("click", function() {      
       // toggle the type attribute
      var type = $('#password').attr('type') === 'password' ? 'text' : 'password';    
      $('#password').attr('type', type);       
       // toggle the eye / eye slash icon       
    });
    
  },
  components: {
    GuestLeftSectionImage
  }
}