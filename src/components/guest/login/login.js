import GuestLeftSectionImage from '../../comman/guest_user/left-section.vue'
import axios from 'axios';
import $ from "jquery";
import { required, minLength, email } from 'vuelidate/lib/validators'

export default {
  name: 'Login',  
  components: {
    GuestLeftSectionImage
  },
  data() {
    return {      
      user: {        
        email: "",
        password: "",        
      },
      submitted: false,
      msg: '',
    };
  },
  validations: {
    user: {        
        email: { required, email },
        password: { required, minLength: minLength(8) },        
    }
  },
  methods: {
    async handleSubmit() {                   
      // form validation
      this.submitted = true;      
      this.$v.$touch();
      if (this.$v.$invalid) {                  
          return;
      }      
      // form validation
      
      // submit form data   
      try {
        let response = await axios.post('auth/login', this.user)
        this.msg = response.data.meta.message;

        if(!response.data.meta.status) {          
          this.$toast.error(this.msg);
          return false;
        }        
      
        localStorage.setItem('user-token',response.data.data.token);
        this.$toast.success(this.msg);
        this.$router.push({name: 'Profile'});
      } catch (error) {
          console.log(error);
      }        
     }           
  },
  created() {
    if(localStorage.getItem('user-token')) {
      this.$router.push({name: 'Profile'});
    }    
  }, 
  mounted: function() {  
    
    $('#togglePassword').on("click", function() {      
       // toggle the type attribute
      var type = $('#password').attr('type') === 'password' ? 'text' : 'password';    
      $('#password').attr('type', type);       
       // toggle the eye / eye slash icon       
    });    
  },
}