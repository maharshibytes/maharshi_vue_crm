import HeaderSectionLayout from '../comman/header-section'
import FooterSectionLayout from '../comman/footer-section'

export default {
  name: 'Profile',  
  components: {
    HeaderSectionLayout,
    FooterSectionLayout
  },
  data() {
    return {            
      submitted: false,      
    };
  },  
  created() {
    let getUserData = localStorage.getItem('user-token');
    if(!getUserData) {
      this.$router.push({name: 'Login'});
    }    
  },  
  methods : {
    logoutUser () {
      localStorage.removeItem('user-token');
      this.$toast.error('You have successfully logged out!');
      this.$router.push({name: 'Login'});    
    }
  } 
}