import VueRouter from 'vue-router'
import Login from '../components/guest/login'
import ForgotPassowrd from '../components/guest/forgot_password'
import Profile from '../components/profile'

const routes = [
  { path: '/' , name: 'Login', component: Login, meta: {plainLayout: true}},
  { path: '/login' , name: 'Login', component: Login, meta: {plainLayout: true}},
  { path: '/forgot-password' , name: 'ForgotPassword', component: ForgotPassowrd, meta: {plainLayout: true}},
  { path: '/profile' , name: 'Profile', component: Profile, meta: {plainLayout: false}},
]

const router = new VueRouter({
  routes,
  mode: 'history'
})

export default router
