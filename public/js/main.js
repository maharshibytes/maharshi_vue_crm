$(document).ready(function() {


   const fileInput = document.getElementById('upload-profile');
   const StoreInput = document.getElementById('img-name');
   fileInput.onchange = () => {
   const selectedFile = fileInput.files[0];
   StoreInput.value = selectedFile.name;
   }


   // Toggle Password Visibility 
   const togglePassword = document.querySelector('#togglePassword');
   const password = document.querySelector('#password');
   togglePassword.addEventListener('click', function (e) {
      // toggle the type attribute
      const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
      password.setAttribute('type', type);
      // toggle the eye / eye slash icon
      this.classList.toggle('show');
   });


  
 
});